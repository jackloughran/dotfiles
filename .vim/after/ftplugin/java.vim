nnoremap <silent> <buffer> <leader>i :JavaImport<cr>
nnoremap <silent> <buffer> <leader>o :JavaImportOrganize<cr>
nnoremap <silent> <buffer> <cr> :JavaSearchContext<cr>
let g:EclimJavaSearchSingleResult = "edit"

setl noexpandtab
setl tabstop=4
setl shiftwidth=4
