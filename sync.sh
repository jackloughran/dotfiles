#!/bin/bash

/usr/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME pull

while true
do
    status=$(/usr/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME status -s)
    if [ -n "$status" ]
    then
	/usr/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME commit -am "automatic commit"
	/usr/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME push
    fi

    sleep 60
done
