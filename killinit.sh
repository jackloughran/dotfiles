#!/bin/bash

for PID in $(cat ~/.initpids)
do
    kill -9 $PID
done
