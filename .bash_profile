export TOMCAT_DIR=/opt/apache-tomcat-7.0.81
export MY_GIT_DIR=~/git
export ACTIVEMQ_DIR=/opt/activemq
export CATALINA_PID="$TOMCAT_DIR/bin/catalina.pid"
export CONSUL_PORT="80"

# ls aliases
alias l='ls -lhrt'
alias la='ls -lhart'


# make the bash prompt better
# first choose colors based on if the branch was modified
function git_color {
	local git_status="$(git status 2> /dev/null)"
	
	if [[ $git_status =~ "nothing to commit" ]]; then
		echo -e "\033[0;94m" # light blue
	elif [[ $git_status =~ "changes added to commit" ]]; then
		echo -e "\033[0;91m" # light red
	elif [[ $git_status =~ "untracked files present" ]]; then
		echo -e "\033[0;31m" # red
	elif [[ $git_status =~ "Changes to be committed" ]]; then
		echo -e "\033[0;92m" # light green
	fi
}

export PS1="\[\e[38;5;244m\]\W\[\e[00m\] \[\$(git_color)\]\$(git rev-parse --abbrev-ref HEAD 2> /dev/null | sed 's/\(.*\)/[\1] /')\[\e[38;5;34m\]$ \[\e[00m\]"


# java deployment stuff
function build {
	rm -rf $TOMCAT_DIR/webapps/*
	buildnodelete $1 $2
}
function buildnodelete {
	if [[ $1 ]]; then
		if [[ $1 == "-f" ]]; then
			serviceDirName=$2
		else
			serviceDirName=${1}-service
		fi
		cd ~/git/$serviceDirName || exit
		mvn clean install -Dmaven.test.skip
		cp $MY_GIT_DIR/$serviceDirName/target/*.war $TOMCAT_DIR/webapps/
		cd - || exit
	else
		echo "usage: $0 [options...] <service>"
		echo "Options:"
		echo "-f	specify full service directory name (default will append \"-service\")"
	fi
}

alias startamq="$ACTIVEMQ_DIR/bin/activemq start"
alias killtomcatthreads='ps -eaf | grep tomcat | grep -v grep | grep -v apache-tomcat-solr | grep -v tmux |  awk '"'"'{print "kill -9 " $2}'"'"' | sh' # credit scott
alias starttomcat="$TOMCAT_DIR/bin/startup.sh"
alias stoptomcat="$TOMCAT_DIR/bin/shutdown.sh 5 -force; killtomcatthreads"
alias resettomcat="stoptomcat; starttomcat"

function deploy {
	/Users/jack.loughran/Documents/scripts/dependency_deployer.sh "$@" &&
	build $1 $2 && resettomcat
}

# scott's env change function
function env {
	if [[ $1 ]]; then
		# gsed -i "s/-Dconsul\.bucket=[^ ]*/-Dconsul.bucket=$1/" /opt/apache-tomcat-7.0.81/bin/startup.sh
		scp -r jack.loughran@$1:/dibs/named_queries/* /dibs/named_queries;
		scp -r jack.loughran@$1:/dibs/settings/* /dibs/settings; 
		sudo scp -r jack.loughran@$1:/dibs/db-service-config/* /dibs/db-service-config;
		sed -i '' "s/zookeeper.host=localhost/zookeeper.host=${1}/g" /dibs/settings/env.properties;
		sed -i '' "s/zookeeper.solr.host=localhost/zookeeper.solr.host=${1}/g" /dibs/settings/env.properties;
		sed -i '' "s/zookeeper.leaderElection.rootNodes=.*/zookeeper.leaderElection.rootNodes=none/g" /dibs/settings/env.properties;
		sed -i '' "s/http:\/\/localhost/http:\/\/${1}/g" /dibs/settings/env.properties;
		sed -i '' "s/solr.read.cloud=localhost:2181/solr.read.cloud=${1}:2181/g" /dibs/settings/inventorySolr.properties;
		sed -i '' "s/solr.write.cloud=localhost:2181/solr.write.cloud=${1}:2181/g" /dibs/settings/inventorySolr.properties;
		sed -i '' "s/solr.reporting.read.cloud=localhost:2181/solr.reporting.read.cloud=${1}:2181/g" /dibs/settings/inventorySolr.properties;
		sed -i '' "s/solr.read.cloud=localhost:2181/solr.read.cloud=${1}:2181/g" /dibs/settings/ecomSolr.properties;
		sed -i '' "s/solr.write.cloud=localhost:2181/solr.write.cloud=${1}:2181/g" /dibs/settings/ecomSolr.properties;
		sed -i '' "s/solr.reporting.read.cloud=localhost:2181/solr.reporting.read.cloud=${1}:2181/g" /dibs/settings/ecomSolr.properties;
		sed -i '' "s/dibs.env=dev/dibs.env=local/g" /dibs/settings/env.properties;
		sed -i '' "s/spring.profiles.active=dev/spring.profiles.active=local/g" /dibs/settings/env.properties;
		sed -i '' "s/grpc.loadbalencer.host=localhost/grpc.loadbalencer.host=${1}/g" /dibs/settings/application.properties;
		sed -i '' "s/^jms.host=.*$/jms.host=localhost:61616/g" /dibs/settings/env.properties;
		sed -i '' "s/^jms.items.host=.*$/jms.items.host=localhost:61616/g" /dibs/settings/env.properties;
		sed -i '' "s/^solr.read.cloud=.*$/solr.read.cloud=${1}:2181/" /dibs/settings/itemSearchSolr.properties;
		sed -i '' "s/^solr.write.cloud=.*$/solr.write.cloud=${1}:2181/" /dibs/settings/itemSearchSolr.properties;
		sed -i '' "s/^solr.reporting.read.cloud=.*$/solr.reporting.read.cloud=${1}:2181/" /dibs/settings/itemSearchSolr.properties;
		sed -i '' "s/log.level=INFO/log.level=DEBUG/" /dibs/settings/env.properties;
		sed -i '' 's/enabled=Y/enabled=N/g' /dibs/settings/jms-consumer.properties;
		sed -i '' 's/cronEnabled=true/cronEnabled=false/' /dibs/settings/env.properties;
	else
		grep "dibs.site.url" /dibs/settings/env.properties | sed "s/dibs.site.url=\(.*\)/\1/"
	fi
}

# correct log indentation
function formatlogs {
	gtac $1 | sed 's/\\n/\'$'\n''/g' | sed 's/\\t/\'$'\t''/g' | sed 's/\\"/\'$'\"''/g' | less
}

# private
# notes
function pushnotes {
	message="update notes"
		if [[ $1 ]]; then
					message="$1"
						fi

							local gitdir=~/git/notes
								git -C $gitdir aa
									git -C $gitdir cm "$message"
										git -C $gitdir push
									}

function note {
	(echo && date | tr -d '\n' | awk '{printf $0 ": "}' && cat) >> ~/git/notes/everything.txt
	pushnotes
}
alias notes="cat ~/git/notes/everything.txt | tr ';' '\n' | fold -s"
function gnotes {
	grep -E "$1" ~/git/notes/everything.txt | tr ';' '\n' | fold -s
}

function version {
	tag="<version>"
	if [[ $1 ]]; then
		tag="${1}>"
	fi
	grep "$tag" pom.xml | head -n 1 | sed -E "s/^.*${tag}([^<]+)<\/.*$/\1/"
}

alias snapshot=/Users/jack.loughran/Documents/scripts/snapshot.sh
alias bump=/Users/jack.loughran/Documents/scripts/bump.sh
alias pull=/Users/jack.loughran/Documents/scripts/pull_request.sh

# version controlling dotfiles
alias config='/usr/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME'

# update bash
alias update="source ~/.bash_profile"

# my aliases
alias db="/Users/jack.loughran/Documents/scripts/mysql.sh"

# git completion
source ~/.git-completion.bash

# add golang programs to path
export PATH=$PATH:~/go/bin

alias grpcurl="grpcurl -plaintext"

alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"

# https://news.ycombinator.com/item?id=10164053
# should preserve history for all tmux windows when tmux is closed
shopt -s histappend

export FZF_DEFAULT_COMMAND='rg --files'

export PATH="$HOME/.cargo/bin:/Users/jack.loughran/Library/Python/2.7/bin:$PATH"

eval "$(starship init bash)"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export EDITOR=vim

alias tcatalina="tail -f $TOMCAT_DIR/logs/catalina.out"

function vpn {
	printf "klj234lknajef0" | pbcopy
	networksetup -connectpppoeservice "1stdib"
}

export TERM=xterm-256color

alias ctags="/usr/local/bin/ctags"
alias format="mvn com.cosium.code:maven-git-code-format:format-code"
alias vim="/usr/local/bin/vim"
