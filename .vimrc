call plug#begin('~/.vim/plugged')

Plug 'plasticboy/vim-markdown'
Plug 'elixir-editors/vim-elixir'
Plug 'mhinz/vim-mix-format'
Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug '/usr/local/opt/fzf'
Plug 'junegunn/fzf.vim'
Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

call plug#end()

" colors
colorscheme gruvbox
let g:gruvbox_contrast_dark = "hard"
let g:lightline = {
      \ 'colorscheme': 'seoul256',
      \ }

" status line
set laststatus=2
set noshowmode

" line numbers
set number
set relativenumber
set numberwidth=3

" indentation
set shiftwidth=2
set tabstop=2
set expandtab
filetype indent on

" misc
syntax on
filetype on
filetype plugin on
set splitright

" vim-markdown
let g:vim_markdown_folding_disabled = 1

" for backspace to work
set backspace=indent,eol,start

" leader stuff
:let mapleader = ","

noremap <leader>p :Files<CR>
noremap <leader>r :Rg 
noremap <leader>w yiw:Rg <C-R>"

" tags
set tags=tags;

" typescript
let g:typescript_indent_disable = 1

" eclim
let g:EclimMavenPomClasspathUpdate = 0

" elixir
let g:mix_format_on_save = 1

" this should make vim work with 256 bit iterm colors
set t_Co=256

" netrw -- from https://shapeshed.com/vim-netrw/
let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 25

" prettier
let g:prettier#autoformat = 0
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue PrettierAsync
