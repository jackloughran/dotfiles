#!/bin/bash

echo "starting notes sync"

~/notes/sync.sh &

NOTES_PID=$!

echo "starting config sync"

~/sync.sh &

CONFIG_PID=$!

echo "saving pids"

echo -e "$GTD_PID\n$NOTES_PID\n$CONFIG_PID" > ~/.initpids

echo "DONE"
