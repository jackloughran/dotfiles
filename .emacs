(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (wombat)))
 '(eclim-eclipse-dirs (quote ("~/eclipse")))
 '(eclim-executable
   "/Users/jack.loughran/.p2/pool/plugins/org.eclim_2.8.0/bin/eclim")
 '(global-visual-line-mode t)
 '(package-selected-packages
   (quote
    (evil-visual-mark-mode alchemist dap-mode lsp-ui company-lsp treemacs projectile use-package protobuf-mode slime ac-emacs-eclim auto-complete eclim paredit cider clojure-mode evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(require 'clojure-mode)
(require 'cider)
(require 'ob-clojure)
(add-hook 'clojure-mode-hook #'paredit-mode)
(add-hook 'cider-repl-mode-hook #'eldoc-mode)
(add-hook 'cider-repl-mode-hook #'paredit-mode)
(setq ring-bell-function #'ignore)
(set-face-attribute 'default nil :height 150) ; value / 10 = pt

; gtd org mode stuff
; much is copied from https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html
(setq org-agenda-files '("~/gtd/inbox.org"
                         "~/gtd/projects.org"
                         "~/gtd/tickler.org"))
;; org-capture
(define-key global-map "\C-cc" 'org-capture)
(setq org-capture-templates '(("t" "Todo [inbox]" entry
                               (file+headline "~/gtd/inbox.org" "Tasks")
                               "* TODO %i%?")
                              ("T" "Tickler" entry
                               (file+headline "~/gtd/tickler.org" "Tickler")
                               "* %i%? \n %U")))
(setq org-refile-targets '(("~/gtd/projects.org" :maxlevel . 3)
                           ("~/gtd/someday.org" :level . 1)
                           ("~/gtd/tickler.org" :maxlevel . 2)))

;; todo keywords
(setq org-todo-keywords '((sequence "TODO" "IN_PROGRESS" "WAITING" "|" "DONE" "CANCELLED")))
;; make it save a timestamp on done
(setq org-log-done t)

;; add agenda shortcut
(global-set-key "\C-ca" 'org-agenda)

(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . dark)) ;; assuming you are using a dark theme
(setq ns-use-proxy-icon nil)
(setq frame-title-format nil)
(tool-bar-mode -1)

;; so S-<left> etc can move you around between many windows
(windmove-default-keybindings)

;; this is required to have a markdown exporter for org mode
;; https://stackoverflow.com/questions/22988092/emacs-org-mode-export-markdown#22990257
(eval-after-load "org"
  '(require 'ox-md nil t))

;; melpa
(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives '("gnu" . (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)

;; SLIME
(setq inferior-lisp-program "/usr/local/bin/sbcl")
(setq slime-contribs '(slime-fancy))

(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'slime-repl-mode-hook (lambda () (paredit-mode +1)))
;; Stop SLIME's REPL from grabbing DEL,
;; which is annoying when backspacing over a '('
(defun override-slime-repl-bindings-with-paredit ()
  (define-key slime-repl-mode-map
    (read-kbd-macro paredit-backward-delete-key) nil))
(add-hook 'slime-repl-mode-hook 'override-slime-repl-bindings-with-paredit)

;; quicklisp
(load (expand-file-name "~/quicklisp/slime-helper.el"))

(global-set-key "\C-cl" 'org-store-link)

; Targets include this file and any file contributing to the agenda - up to 5 levels deep
(setq org-refile-targets '((nil :maxlevel . 5) (org-agenda-files :maxlevel . 5)))
(setq initial-major-mode 'org-mode)
(setq initial-scratch-message nil)

; evil mode
(setq evil-want-C-u-scroll t)
(require 'evil)
(evil-mode 1)

; evil-org-mode
(add-to-list 'load-path "~/.emacs.d/plugins/evil-org")
(require 'evil-org)
(add-hook 'org-mode-hook 'evil-org-mode)
(evil-org-set-key-theme '(navigation insert textobjects additional calendar))

(require 'cc-mode)

(condition-case nil
    (require 'use-package)
  (file-error
   (require 'package)
   (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
   (package-initialize)
   (package-refresh-contents)
   (package-install 'use-package)
   (require 'use-package)))

					; elixir
(add-hook `elixir-mode-hook (lambda () (company-mode) (alchemist-mode) (setq company-backends '((alchemist-company :with company-yasnippet)))))
(setq alchemist-goto-elixir-source-dir "~/git/elixir/")
;; Create a buffer-local hook to run elixir-format on save, only when we enable elixir-mode.
(add-hook 'elixir-mode-hook
          (lambda () (add-hook 'before-save-hook 'elixir-format nil t)))
