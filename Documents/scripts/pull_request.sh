#!/bin/sh

BRANCH=$(git rev-parse --abbrev-ref HEAD 2> /dev/null)

REPO=$(basename `git rev-parse --show-toplevel`)

open "https://github.com/1stdibs/$REPO/pull/new/$BRANCH"
